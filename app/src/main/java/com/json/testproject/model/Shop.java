package com.json.testproject.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.json.testproject.providers.ShopsContentProvider;

/**
 * Created by demo on 12.06.15.
 */

/* {
            "abilities": [
                "buy_phones",
                "connect",
                "crediting",
                "get_bills",
                "mtsmoney",
                "pay"
            ],
            "address": "\u0422\u0443\u043b\u044c\u0441\u043a\u0430\u044f \u043e\u0431\u043b, \u041d\u043e\u0432\u043e\u043c\u043e\u0441\u043a\u043e\u0432\u0441\u043a \u0433, \u041c\u043e\u0441\u043a\u043e\u0432\u0441\u043a\u0430\u044f \u0443\u043b, \u0434\u043e\u043c \u2116 10\u0430",
            "city": 13252,
            "code": "H222",
            "district_name": "",
            "district_type": "",
            "email": "H222@mtsretail.ru",
            "house": "10\u0430",
            "housing": "",
            "id": 71499,
            "index": "301650",
            "latitude": "54.9808",
            "longitude": "33.3423",
            "metro": null,
            "name": "\u0422\u0443\u043b\u0430 \u041d\u043e\u0432\u043e\u043c\u043e\u0441\u043a\u043e\u0432\u0441\u043a 01-\u0427\u0417",
            "nearest": [
                73258,
                72329,
                72476
            ],
            "opening_hours": "\u041f\u041d-\u041f\u0422 09:00-20:00, \u0421\u0411-\u0412\u0421 10:00-19:00",
            "options": [
                "acquiring"
            ],
            "street_name": "\u041c\u043e\u0441\u043a\u043e\u0432\u0441\u043a\u0430\u044f",
            "street_type": "\u0443\u043b",
            "type": "\u041e\u041f",
            "zoom": 19
        }*/
public class Shop implements ContentValuable {

    private String abilities = "";
    private String address;
    private int city;
    private String code;
    private String district_name;
    private String district_type;
    private String email;
    private String house;
    private String housing;
    private int id;
    private String index;
    private String latitude;
    private String longitude;
    private String name;
    private String nearest;
    private String opening_hours;
    private String options;
    private String street_name;
    private String street_type;
    private String type;
    private int zoom;

    public String getAbilities() {
        return abilities;
    }

    public Shop() {

    }

    public Shop(Cursor cursor) {
        this.address = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.ADDRESS));
        this.name = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.NAME));
        this.code = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.CODE));
        this.house = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.HOUSE));
        this.housing = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.HOUSING));
        this.street_name = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.STREET_NAME));
        this.street_type = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.STREET_TYPE));
        this.latitude = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.LONGITUDE));
        this.district_name = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.DISTRICT_NAME));
        this.district_type = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.DISTRICT_TYPE));
        this.type = cursor.getString(cursor.getColumnIndex(ShopsContentProvider.TYPE));
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(ShopsContentProvider.SHOP_ID, id);
        cv.put(ShopsContentProvider.ABILITIES, abilities);
        cv.put(ShopsContentProvider.ADDRESS, address);
        cv.put(ShopsContentProvider.CITY, city);
        cv.put(ShopsContentProvider.CODE, code);
        cv.put(ShopsContentProvider.DISTRICT_NAME, district_name);
        cv.put(ShopsContentProvider.DISTRICT_TYPE, district_type);
        cv.put(ShopsContentProvider.EMAIL, email);
        cv.put(ShopsContentProvider.HOUSE, house);
        cv.put(ShopsContentProvider.HOUSING, housing);
        cv.put(ShopsContentProvider.INDEX, index);
        cv.put(ShopsContentProvider.LATITUDE, latitude);
        cv.put(ShopsContentProvider.LONGITUDE, longitude);
        cv.put(ShopsContentProvider.NAME, name);
        cv.put(ShopsContentProvider.NEAREST, nearest);
        cv.put(ShopsContentProvider.OPENING_HOURS, opening_hours);
        cv.put(ShopsContentProvider.OPTIONS, options);
        cv.put(ShopsContentProvider.STREET_NAME, street_name);
        cv.put(ShopsContentProvider.STREET_TYPE, street_type);
        cv.put(ShopsContentProvider.TYPE, type);
        cv.put(ShopsContentProvider.ZOOM, zoom);
        return cv;
    }

    public void setAbilities(String abilities) {
        this.abilities = abilities;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getDistrict_type() {
        return district_type;
    }

    public void setDistrict_type(String district_type) {
        this.district_type = district_type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getHousing() {
        return housing;
    }

    public void setHousing(String housing) {
        this.housing = housing;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNearest() {
        return nearest;
    }

    public void setNearest(String nearest) {
        this.nearest = nearest;
    }

    public String getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(String opening_hours) {
        this.opening_hours = opening_hours;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getStreet_type() {
        return street_type;
    }

    public void setStreet_type(String street_type) {
        this.street_type = street_type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }
}
