package com.json.testproject.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.json.testproject.providers.RegionsContentProvider;

/**
 * Created by user on 12/6/2015.
 */
public class City implements ContentValuable{

    /*{
            "central": 72586,
            "city_type": "\u0433",
            "id": 13221,
            "name": "\u0422\u0430\u0433\u0430\u043d\u0440\u043e\u0433",
            "region_id": 1798
        }*/

    private int central;
    private String city_type;
    private int id;
    private String name;
    private int region_id;

    public City() {

    }

    public City(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(RegionsContentProvider.CITY_ID));
        this.central = cursor.getInt(cursor.getColumnIndex(RegionsContentProvider.CENTRAL));
        this.region_id = cursor.getInt(cursor.getColumnIndex(RegionsContentProvider.REGION_ID));
        this.city_type = cursor.getString(cursor.getColumnIndex(RegionsContentProvider.CITY_TYPE));
        this.name = cursor.getString(cursor.getColumnIndex(RegionsContentProvider.NAME));
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(RegionsContentProvider.CITY_ID, id);
        cv.put(RegionsContentProvider.NAME, name);
        cv.put(RegionsContentProvider.REGION_ID, region_id);
        cv.put(RegionsContentProvider.CENTRAL, central);
        cv.put(RegionsContentProvider.CITY_TYPE, city_type);
        return cv;
    }

    public int getCentral() {
        return central;
    }

    public void setCentral(int central) {
        this.central = central;
    }

    public String getCity_type() {
        return city_type;
    }

    public void setCity_type(String city_type) {
        this.city_type = city_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRegion_id() {
        return region_id;
    }

    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }
}
