package com.json.testproject.model;

import android.content.ContentValues;

/**
 * Created by demo on 13.06.15.
 */
public interface ContentValuable {

    public ContentValues toContentValues();
}
