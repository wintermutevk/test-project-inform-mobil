package com.json.testproject.model;

import android.content.ContentValues;

import com.json.testproject.providers.RegionsContentProvider;

/**
 * Created by demo on 12.06.15.
 */
public class Region implements ContentValuable {
    /*{
            "center": 14594,
            "id": 1945119,
            "name": "\u0411\u0430\u043b\u0430\u0448\u0438\u0445\u0430",
            "name_simple": ""
        }*/

    private int center;
    private int id;
    private String name;
    private String name_simple;

    public int getCenter() {
        return center;
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(RegionsContentProvider.REGION_ID, id);
        cv.put(RegionsContentProvider.NAME, name);
        cv.put(RegionsContentProvider.CENTER, center);
        cv.put(RegionsContentProvider.NAME_SIMPLE, name_simple);
        return cv;
    }

    public void setCenter(int center) {
        this.center = center;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_simple() {
        return name_simple;
    }

    public void setName_simple(String name_simple) {
        this.name_simple = name_simple;
    }
}
