package com.json.testproject;

/**
 * Created by demo on 12.06.15.
 */
public class Const {
    public static final int DB_VERSION = 1;

    public static class ACTION {
        public static final String SYNC_ASSETS = "com.json.assets.sync_assets";
    }
}
