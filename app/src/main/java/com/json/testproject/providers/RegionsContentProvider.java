package com.json.testproject.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.json.testproject.Const;

import java.sql.SQLException;

/**
 * Created by demo on 13.06.15.
 */
public class RegionsContentProvider extends ContentProvider {

    private static final String DB_NAME = "regions_db";
    private static final int DB_VERSION = Const.DB_VERSION;
    public static final String TABLE_REGIONS = "regions_table";
    public static final String TABLE_CITIES = "cities_table";

    // common fields
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String REGION_ID = "region_id";

    // regions
    public static final String CENTER = "center";
    public static final String NAME_SIMPLE = "name_simple";

    // cities
    public static final String CITY_TYPE = "city_type";
    public static final String CENTRAL = "central";
    public static final String CITY_ID = "city_id";

    private final static String CREATE_TABLE_REGIONS = "create table " + TABLE_REGIONS + " ("
            + ID + " integer primary key autoincrement, "
            + REGION_ID + " integer default -1, "
            + CENTER + " integer default -1, "
            + NAME + " text default \'\', "
            + NAME_SIMPLE + " text default \'\'"
            + ");";

    private final static String CREATE_TABLE_CITIES = "create table " + TABLE_CITIES + " ("
            + ID + " integer primary key autoincrement, "
            + CITY_ID + " integer default -1, "
            + CENTRAL + " integer default -1, "
            + REGION_ID + " integer default -1, "
            + NAME + " text default \'\', "
            + CITY_TYPE + " text default \'\'"
            + ");";

    public final static String AUTHORITY = "com.json.testproject.providers.RegionsContentProvider";

    public final static Uri URI_REGIONS = Uri.parse("content://" + AUTHORITY + "/" + TABLE_REGIONS);
    public final static Uri URI_CITIES = Uri.parse("content://" + AUTHORITY + "/" + TABLE_CITIES);

    private final static String DROP_TABLE_REGIONS = "drop table if exists " + TABLE_REGIONS;
    private final static String DROP_TABLE_CITIES = "drop table if exists " + TABLE_CITIES;

    private final static int MATCH_REGIONS_ITEM = 0;
    private final static int MATCH_REGIONS_DIR = 1;
    private final static int MATCH_LIKE = 2;
    private final static int MATCH_CITIES_ITEM = 3;
    private final static int MATCH_CITIES_DIR = 4;

    private UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH) {{
        addURI(AUTHORITY, TABLE_REGIONS + "/#", MATCH_REGIONS_ITEM);
        addURI(AUTHORITY, TABLE_REGIONS, MATCH_REGIONS_DIR);
        addURI(AUTHORITY, TABLE_REGIONS + "/like/*", MATCH_LIKE);
        addURI(AUTHORITY, TABLE_CITIES + "/#", MATCH_CITIES_ITEM);
        addURI(AUTHORITY, TABLE_CITIES, MATCH_CITIES_DIR);
    }};

    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        dbOpenHelper = new DbOpenHelper(getContext());
        return true;
    }

    private Cursor getLikeSelection(Uri uri) {
        String what = uri.getLastPathSegment();
        db = dbOpenHelper.getReadableDatabase();
        String selection = "SELECT DISTINCT * FROM " + TABLE_REGIONS + " AS RG LEFT JOIN " + TABLE_CITIES
                + " AS CT ON RG.region_id = CT.region_id WHERE CT.name LIKE '" + what + "%'";
        Cursor cursor = db.rawQuery(selection, null);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case MATCH_LIKE:
                return getLikeSelection(uri);
            case MATCH_REGIONS_DIR:
                queryBuilder.setTables(TABLE_REGIONS);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ID + " ASC";
                }
                break;
            case MATCH_REGIONS_ITEM:
                queryBuilder.setTables(TABLE_REGIONS);
                queryBuilder.appendWhere(ID + "=" + uri.getLastPathSegment());
                break;
            case MATCH_CITIES_DIR:
                queryBuilder.setTables(TABLE_CITIES);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ID + " ASC";
                }
                break;
            case MATCH_CITIES_ITEM:
                queryBuilder.setTables(TABLE_CITIES);
                queryBuilder.appendWhere(ID + "=" + uri.getLastPathSegment());
                break;
        }
        Cursor cursor = queryBuilder.query(dbOpenHelper.getReadableDatabase(), projection, selection, null, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table = "";
        switch (URI_MATCHER.match(uri)) {
            case MATCH_REGIONS_DIR:
                table = TABLE_REGIONS;
                break;
            case MATCH_CITIES_DIR:
                table = TABLE_CITIES;
                break;
            case MATCH_REGIONS_ITEM:
            case MATCH_CITIES_ITEM:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
        }
        db = dbOpenHelper.getWritableDatabase();
        long rowId = db.insert(table, null, values);
        Uri insertUri = ContentUris.withAppendedId(uri, rowId);
        getContext().getContentResolver().notifyChange(insertUri, null);
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table = "";
        switch (URI_MATCHER.match(uri)) {
            case MATCH_CITIES_DIR:
                table = TABLE_CITIES;
                break;
            case MATCH_REGIONS_DIR:
                table = TABLE_REGIONS;
                break;
        }
        db = dbOpenHelper.getWritableDatabase();
        int affectedRowCount = db.delete(table, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return affectedRowCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String table = "";
        switch (URI_MATCHER.match(uri)) {
            case MATCH_REGIONS_DIR:
            case MATCH_CITIES_DIR:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
            case MATCH_REGIONS_ITEM:
                table = TABLE_REGIONS;
                selection = ID + "=" + uri.getLastPathSegment();
                break;
            case MATCH_CITIES_ITEM:
                table = TABLE_CITIES;
                selection = ID + "=" + uri.getLastPathSegment();
                break;
        }
        db = dbOpenHelper.getWritableDatabase();
        int rowsAffected = db.update(table, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case MATCH_REGIONS_DIR:
                table = TABLE_REGIONS;
                break;
            case MATCH_CITIES_DIR:
                table = TABLE_CITIES;
                break;
            default:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
        }
        db = dbOpenHelper.getWritableDatabase();
        db.beginTransaction();

        try {
            for (ContentValues cv : values) {
                long newId = db.insertOrThrow(table, null, cv);
                if (newId <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            db.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return values.length;
    }

    private class DbOpenHelper extends SQLiteOpenHelper {

        public DbOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_REGIONS);
            db.execSQL(CREATE_TABLE_CITIES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_TABLE_REGIONS);
            db.execSQL(DROP_TABLE_CITIES);
            onCreate(db);
        }
    }
}
