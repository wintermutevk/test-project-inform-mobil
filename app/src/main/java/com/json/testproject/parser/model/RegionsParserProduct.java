package com.json.testproject.parser.model;

import com.json.testproject.model.City;
import com.json.testproject.model.Region;

import java.util.List;

/**
 * Created by demo on 13.06.15.
 */
public class RegionsParserProduct implements ParserProduct {

    private List<City> cities;
    private List<Region> regions;

    public RegionsParserProduct(List<City> cities, List<Region> regions) {
        this.cities = cities;
        this.regions = regions;
    }

    public List<City> getCities() {
        return cities;
    }

    public List<Region> getRegions() {
        return regions;
    }

    @Override
    public void release() {
        if(cities != null) {
            cities.clear();
        }
        if(regions != null) {
            regions.clear();
        }
    }
}
