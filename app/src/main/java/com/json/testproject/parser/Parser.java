package com.json.testproject.parser;

import com.json.testproject.parser.model.ParserProduct;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by demo on 13.06.15.
 */
public interface Parser {
    public enum Type {
        SHOPS, REGIONS
    }

    public ParserProduct parse(InputStream in) throws IOException;
}
