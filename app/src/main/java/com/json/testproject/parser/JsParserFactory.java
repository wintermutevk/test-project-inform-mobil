package com.json.testproject.parser;

/**
 * Created by demo on 13.06.15.
 */
public class JsParserFactory {

    public static Parser getParser(Parser.Type type) {
        switch (type) {
            case SHOPS:
                return new ShopsParser();
            case REGIONS:
                return new RegionsParser();
        }
        return null;
    }

}
