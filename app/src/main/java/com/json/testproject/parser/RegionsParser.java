package com.json.testproject.parser;

import android.util.Log;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.json.testproject.model.City;
import com.json.testproject.model.Region;
import com.json.testproject.parser.model.ParserProduct;
import com.json.testproject.parser.model.RegionsParserProduct;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by demo on 13.06.15.
 */
public class RegionsParser implements Parser {

    private City city;
    private Region region;

    private List<City> cities;
    private List<Region> regions;

    @Override
    public ParserProduct parse(InputStream in) throws IOException {

        cities = new ArrayList<>();
        regions = new ArrayList<>();

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jParser = jsonFactory.createParser(in);

        jParser.nextToken();
        if(jParser.getCurrentToken() == JsonToken.START_OBJECT) {
            jParser.nextToken();
            while (true) {
                String arrayName = jParser.getCurrentName();
                jParser.nextToken();
                if("city".equals(arrayName)) {
                    do {
                        jParser.nextToken();
                        parseCityArray(jParser);
                    } while (jParser.getCurrentToken() != JsonToken.END_ARRAY);
                    jParser.nextToken();
                } else if("region".equals(arrayName)) {
                    Log.d("tag", "region found");
                    do {
                        jParser.nextToken();
                        parseRegionArray(jParser);
                    } while (jParser.getCurrentToken() != JsonToken.END_ARRAY);
                    jParser.nextToken();
                } else {
                    break;
                }
            }


        }
        return new RegionsParserProduct(cities, regions);
    }

    private void parseCityArray(JsonParser jParser) throws IOException {
        if(jParser.getCurrentToken() == JsonToken.START_OBJECT) {
            city = new City();
        }
        if(jParser.getCurrentToken() == JsonToken.END_OBJECT) {
            cities.add(city);
        }
        if("central".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            city.setCentral(jParser.getIntValue());
        }
        if("city_type".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            city.setCity_type(jParser.getValueAsString());
        }
        if("id".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            city.setId(jParser.getIntValue());
        }
        if("region_id".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            city.setRegion_id(jParser.getIntValue());
        }
        if("name".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            city.setName(jParser.getValueAsString());
        }
    }

    private void parseRegionArray(JsonParser jParser) throws IOException {
        if(jParser.getCurrentToken() == JsonToken.START_OBJECT) {
            region = new Region();
        }
        if(jParser.getCurrentToken() == JsonToken.END_OBJECT) {
            regions.add(region);
        }
        if("center".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            region.setCenter(jParser.getIntValue());
        }
        if("id".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            region.setId(jParser.getIntValue());
        }
        if("name".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            region.setName(jParser.getValueAsString());
        }
        if("name_simple".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            region.setName_simple(jParser.getValueAsString());
        }
    }
}
