package com.json.testproject.parser.model;

import com.json.testproject.model.Shop;

import java.util.List;

/**
 * Created by demo on 13.06.15.
 */
public class ShopsParserProduct implements ParserProduct {
    private List<Shop> shops;

    public ShopsParserProduct(List<Shop> shops) {
        this.shops = shops;
    }

    public List<Shop> getShops() {
        return shops;
    }


    @Override
    public void release() {
        if(shops != null) {
            shops.clear();
        }
    }
}
