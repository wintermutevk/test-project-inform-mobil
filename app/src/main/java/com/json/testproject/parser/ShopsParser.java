package com.json.testproject.parser;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.json.testproject.model.Shop;
import com.json.testproject.parser.model.ParserProduct;
import com.json.testproject.parser.model.ShopsParserProduct;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by demo on 13.06.15.
 */
public class ShopsParser implements Parser {

    private Shop shop;
    private List<Shop> shops;

    @Override
    public ParserProduct parse(InputStream in) throws IOException {

        shops = new ArrayList<>();

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jParser = jsonFactory.createParser(in);

        jParser.nextToken();
        if(jParser.getCurrentToken() == JsonToken.START_OBJECT) {
            jParser.nextToken();
            String arrayName = jParser.getCurrentName();
            jParser.nextToken();
            if("shop".equals(arrayName)) {
                do {
                    jParser.nextToken();
                    parseShopsArray(jParser);
                } while (!(jParser.getCurrentToken() == null && jParser.nextToken() == null));
                jParser.nextToken();
            }

        }
        return new ShopsParserProduct(shops);
    }

    private void parseShopsArray(JsonParser jParser) throws IOException {
        if(jParser.getCurrentToken() == JsonToken.START_OBJECT) {
            shop = new Shop();
        }
        if(jParser.getCurrentToken() == JsonToken.END_OBJECT) {
            shops.add(shop);
        }
        if("abilities".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            StringBuilder sb = new StringBuilder();
            while(true) {
                jParser.nextToken();
                String value = jParser.getValueAsString();
                if(value != null) {
                    sb.append(value);
                } else {
                    break;
                }
                sb.append(",");
            }
        }
        if("address".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setAddress(jParser.getValueAsString());
        }
        if("city".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setCity(jParser.getIntValue());
        }
        if("code".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setCode(jParser.getValueAsString());
        }
        if("district_name".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setDistrict_name(jParser.getValueAsString());
        }
        if("district_type".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setDistrict_type(jParser.getValueAsString());
        }
        if("email".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setEmail(jParser.getValueAsString());
        }
        if("house".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setHouse(jParser.getValueAsString());
        }
        if("housing".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setHousing(jParser.getValueAsString());
        }
        if("id".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setId(jParser.getIntValue());
        }
        if("index".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setIndex(jParser.getValueAsString());
        }
        if("latitude".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setLatitude(jParser.getValueAsString());
        }
        if("longitude".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setLongitude(jParser.getValueAsString());
        }
        if("name".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setName(jParser.getValueAsString());
        }
        if("opening_hours".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setOpening_hours(jParser.getValueAsString());
        }
        if("street_name".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setStreet_name(jParser.getValueAsString());
        }
        if("street_type".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setStreet_type(jParser.getValueAsString());
        }
        if("type".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setType(jParser.getValueAsString());
        }
        if("zoom".equals(jParser.getCurrentName())) {
            jParser.nextToken();
            shop.setZoom(jParser.getIntValue());
        }
    }
}
