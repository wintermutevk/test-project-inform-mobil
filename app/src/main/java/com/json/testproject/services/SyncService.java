package com.json.testproject.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Debug;
import android.util.Log;

import com.json.testproject.Const;
import com.json.testproject.model.ContentValuable;
import com.json.testproject.parser.JsParserFactory;
import com.json.testproject.parser.Parser;
import com.json.testproject.parser.model.RegionsParserProduct;
import com.json.testproject.parser.model.ShopsParserProduct;
import com.json.testproject.providers.RegionsContentProvider;
import com.json.testproject.providers.ShopsContentProvider;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by demo on 13.06.15.
 */
public class SyncService extends IntentService {

    public SyncService() {
        super("");
    }

    public SyncService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getExtras().getString("action");
        if(action.equals(Const.ACTION.SYNC_ASSETS)) {
            try {
                RegionsParserProduct regions = (RegionsParserProduct) JsParserFactory.getParser(Parser.Type.REGIONS).parse(getAssets().open("regions.json"));
                saveList(RegionsContentProvider.URI_REGIONS, regions.getRegions());
                saveList(RegionsContentProvider.URI_CITIES, regions.getCities());
                regions.release();

                ShopsParserProduct shops = (ShopsParserProduct) JsParserFactory.getParser(Parser.Type.SHOPS).parse(getAssets().open("shops.json"));
                saveList(ShopsContentProvider.URI_DATA, shops.getShops());
                shops.release();

/*
                Cursor cursor1 = getContentResolver().query(ShopsContentProvider.URI_DATA, null, null, null, null);
                Log.d("tag", "shops cursor " + cursor1.getCount());

                Cursor cursor2 = getContentResolver().query(RegionsContentProvider.URI_CITIES, null, null, null, null);
                Log.d("tag", "cities cursor " + cursor2.getCount());

                Cursor cursor3 = getContentResolver().query(RegionsContentProvider.URI_REGIONS, null, null, null, null);
                Log.d("tag", "regions cursor " + cursor3.getCount());*/

                SyncService.logHeap();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void saveList(Uri uri, List<? extends ContentValuable> list) {
        ContentValues[] cvs = new ContentValues[list.size()];
        for(int i=0; i<list.size(); i++) {
            cvs[i] = list.get(i).toContentValues();
        }
        getContentResolver().bulkInsert(uri, cvs);
    }

    public static void logHeap() {
        Double allocated = new Double(Debug.getNativeHeapAllocatedSize())/new Double((1048576));
        Double available = new Double(Debug.getNativeHeapSize())/1048576.0;
        Double free = new Double(Debug.getNativeHeapFreeSize())/1048576.0;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);

        Log.d("tag", "debug. =================================");
        Log.d("tag", "debug.heap native: allocated " + df.format(allocated) + "MB of " + df.format(available) + "MB (" + df.format(free) + "MB free)");
        Log.d("tag", "debug.memory: allocated: " + df.format(new Double(Runtime.getRuntime().totalMemory()/1048576)) + "MB of " + df.format(new Double(Runtime.getRuntime().maxMemory()/1048576))+ "MB (" + df.format(new Double(Runtime.getRuntime().freeMemory()/1048576)) +"MB free)");
    }

}
