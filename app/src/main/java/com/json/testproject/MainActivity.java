package com.json.testproject;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.json.testproject.fragments.RegionsFragment;
import com.json.testproject.providers.RegionsContentProvider;
import com.json.testproject.services.SyncService;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Cursor cursor = getContentResolver().query(RegionsContentProvider.URI_REGIONS, null, null, null, null);
        if(!cursor.moveToFirst()) {
            Intent intent = new Intent(this, SyncService.class);
            intent.putExtra("action", Const.ACTION.SYNC_ASSETS);
            startService(intent);
        }
        cursor.close();
        RegionsFragment regionsFragment = new RegionsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.rlFragmentContainer, regionsFragment).commit();

    }

}
