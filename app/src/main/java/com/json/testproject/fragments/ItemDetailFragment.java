package com.json.testproject.fragments;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.json.testproject.R;
import com.json.testproject.model.Shop;
import com.json.testproject.providers.ShopsContentProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by demo on 14.06.15.
 */
public class ItemDetailFragment extends Fragment {

    private long id;

    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("position")) {
            id = getArguments().getInt("position");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_item_detail, container, false);
        ListView lvInfo = (ListView) v.findViewById(R.id.lvInfo);
        List<String> list = new ArrayList<>();
        Uri contentUri = ContentUris.withAppendedId(ShopsContentProvider.URI_DATA, id);
        Cursor c = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                Shop shop = new Shop(c);
                list.add("Адрес : " + shop.getAddress());
                list.add("Имя : " + shop.getName());
                list.add("Код : " + shop.getCode());
                list.add("Дом : " + shop.getHouse());
                list.add("Улица : " + shop.getStreet_name());
                list.add("Широта : " + shop.getLatitude());
                list.add("Долгота : " + shop.getLongitude());
                list.add("Тип : " + shop.getType());
            }
            c.close();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
        lvInfo.setAdapter(adapter);

        return v;
    }
}
