package com.json.testproject.fragments;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.json.testproject.R;
import com.json.testproject.adapter.RegionsAdapter;
import com.json.testproject.model.City;
import com.json.testproject.providers.RegionsContentProvider;

import java.util.HashMap;

/**
 * Created by demo on 13.06.15.
 */
public class RegionsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ExpandableListView.OnChildClickListener{

    private RegionsAdapter adapter;
    private ExpandableListView elvRegions;
    private String[] groupFrom = new String[]{RegionsContentProvider.NAME};
    private int[] groupTo = new int[]{android.R.id.text1};

    private String[] childFrom = new String[]{RegionsContentProvider.NAME};
    private int[] childTo = new int[]{android.R.id.text1};

    private String previousText = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_regions, container, false);
        elvRegions = (ExpandableListView) v.findViewById(R.id.elvRegions);
        adapter = new RegionsAdapter(this, android.R.layout.simple_expandable_list_item_1, groupFrom, groupTo,
                android.R.layout.simple_list_item_1, childFrom, childTo);
        elvRegions.setAdapter(adapter);
        elvRegions.setOnChildClickListener(this);



        // Prepare the loader. Either re-connect with an existing one,
        // or start a new one.
        Loader loader = getActivity().getSupportLoaderManager().getLoader(-1);
        if (loader != null && !loader.isReset()) {
            getActivity().getSupportLoaderManager().restartLoader(-1, null, this);
        } else {
            getActivity().getSupportLoaderManager().initLoader(-1, null, this);
        }
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(final String query) {

                    /*Bundle bundle = new Bundle();
                    bundle.putString("query", query);
                    getLoaderManager().restartLoader(-1, bundle, RegionsFragment.this);*/

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    Bundle bundle = new Bundle();
                    bundle.putString("query", newText);
                    getActivity().getSupportLoaderManager().restartLoader(-1, bundle, RegionsFragment.this);

                    if(newText.length() > 1 || previousText.length() > newText.length()) {
                        adapter.setConstraint(newText);
                        for(int i=0; i<adapter.getGroupCount(); i++) {
                            //elvRegions.collapseGroup(i);
                            elvRegions.expandGroup(i);
                        }

                    }
                    previousText = newText;

                    return true;
                }
            });
        }
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
        Cursor cursor = adapter.getChild(groupPosition, childPosition);
        if(cursor.moveToFirst()) {
            City city = new City(cursor);
            MasterPagerFragment masterPagerFragment = new MasterPagerFragment();
            Bundle args = new Bundle();
            args.putInt("city_id", city.getId());
            masterPagerFragment.setArguments(args);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.rlFragmentContainer, masterPagerFragment).addToBackStack(null).commit();
        }

        /*Cursor cursor = adapter.getChild(groupPosition, childPosition);
        if(cursor.moveToFirst()) {
            City city = new City(cursor);
            Log.d("tag", "city name is: " + city.getName() + ", region " + city.getRegion_id());
        }*/
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.
        CursorLoader cl;
        String query = "";
        if(args != null) {
            query = args.getString("query");
        }
        if (id != -1) {
            // child cursor

            cl = new CursorLoader(getActivity(), RegionsContentProvider.URI_CITIES,
                    null, RegionsContentProvider.REGION_ID + "=" + id + " AND " + RegionsContentProvider.NAME + " LIKE '" + query + "%'", null, null);
        } else {
            // group cursor
            Uri uri = Uri.withAppendedPath(RegionsContentProvider.URI_REGIONS, TextUtils.isEmpty(query) ? "" : "like/" + query);
            cl = new CursorLoader(getActivity(), uri, null, null, null, null);
        }
        return cl;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Swap the new cursor in.
        int id = loader.getId();
        if (id != -1) {
            // child cursor
            if (!data.isClosed()) {

                HashMap<Integer,Integer> groupMap = adapter.getGroupMap();
                try {
                    int groupPos = groupMap.get(id);

                    adapter.setChildrenCursor(groupPos, data);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        } else {
            adapter.setGroupCursor(data);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // is about to be closed.
        int id = loader.getId();
        if (id != -1) {
            // child cursor
            try {
                adapter.setChildrenCursor(id, null);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            adapter.setGroupCursor(null);
        }
    }

}
