package com.json.testproject.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.json.testproject.R;
import com.json.testproject.providers.ShopsContentProvider;

/**
 * Created by demo on 14.06.15.
 */
public class MasterPagerFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ViewPager mPager;

    private CursorPagerAdapter adapter;

    private int cityId = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey("city_id")) {
            cityId = getArguments().getInt("city_id");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_pager, container, false);

        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        adapter = new CursorPagerAdapter(getFragmentManager(), null);
        mPager.setAdapter(adapter);

        getLoaderManager().initLoader(0, null, this);
        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader loader = new CursorLoader(getActivity(),
                ShopsContentProvider.URI_DATA,
                new String[]{ShopsContentProvider.ID, ShopsContentProvider.SHOP_ID},
                ShopsContentProvider.CITY + "=" + cityId,
                null,
                null);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    private class CursorPagerAdapter extends FragmentStatePagerAdapter {

        private Cursor mCursor;

        public CursorPagerAdapter(FragmentManager fm, Cursor c) {
            super(fm);
            mCursor = c;
        }

        @Override
        public Fragment getItem(int position) {
            if (mCursor.moveToPosition(position)) {
                int id = mCursor.getInt(mCursor.getColumnIndex(ShopsContentProvider.ID));
                Bundle arguments = new Bundle();
                arguments.putInt("position", id);
                ItemDetailFragment fragment = new ItemDetailFragment();
                fragment.setArguments(arguments);
                return fragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            if (mCursor != null) {
                return mCursor.getCount();
            }
            return 0;
        }

        public void swapCursor(Cursor cursor) {
            mCursor = cursor;
            notifyDataSetChanged();
        }
    }

}
