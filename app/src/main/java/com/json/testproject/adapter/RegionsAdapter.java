package com.json.testproject.adapter;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.widget.SimpleCursorTreeAdapter;

import com.json.testproject.fragments.RegionsFragment;
import com.json.testproject.providers.RegionsContentProvider;

import java.util.HashMap;

/**
 * Created by demo on 13.06.15.
 */
public class RegionsAdapter extends SimpleCursorTreeAdapter {

    private HashMap<Integer, Integer> groupMap;
    private RegionsFragment fragment;
    private String constraint = "";

    public RegionsAdapter(RegionsFragment fragment,
                          int groupLayout, String[] groupFrom, int[] groupTo,
                          int childLayout, String[] childFrom, int[] childTo) {
        super(fragment.getActivity(), null, groupLayout, groupFrom, groupTo, childLayout, childFrom, childTo);
        groupMap = new HashMap<>();
        this.fragment = fragment;
    }

    public void setConstraint(String constraint) {
        this.constraint = constraint;
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {

        int groupPos = groupCursor.getPosition();
        int groupId = groupCursor.getInt(groupCursor.getColumnIndex(RegionsContentProvider.REGION_ID));

        groupMap.put(groupId, groupPos);

        Bundle bundle = new Bundle();
        bundle.putString("query", constraint);

        Loader loader = fragment.getActivity().getSupportLoaderManager().getLoader(groupId);
        if (loader != null && !loader.isReset() ) {
            fragment.getActivity().getSupportLoaderManager().restartLoader(groupId, bundle, fragment);
        } else {
            fragment.getActivity().getSupportLoaderManager().initLoader(groupId, bundle, fragment);
        }

        return null;
    }

    //Accessor method
    public HashMap<Integer, Integer> getGroupMap() {
        return groupMap;
    }
}
